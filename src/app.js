const express = require('express');
require('./db/mongoose')
const questionsRouter = require('./routers/questions')

const app = express();

app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(questionsRouter)

module.exports = app