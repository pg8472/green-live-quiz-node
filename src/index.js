const express = require('express')
const app = require('./app')
const path = require('path')
const http = require('http')
const axios = require('axios')
const socketio = require('socket.io')
const Filter = require('bad-words')
const fetch = require('node-fetch')

const server = http.createServer(app)
const io = socketio(server)
const port = process.env.PORT;

const publicDirPath = path.join(__dirname, '../public')
app.use(express.static(publicDirPath))

io.on('connection', (socket) =>{
    console.log('New WebSocket connection')

    socket.on('sendMessage', (message, callback) =>{

        axios({
            method: 'post',
            url: process.env.AXIOS_URL + '/questions',
            data: {
                questionAsked: message,
            }
        });

        console.log('Message:', message)
        callback()
    })

    
    socket.on('setPageLoad', (message) =>{
        axios.get('http://localhost:3000/questions')
        .then(res =>{
            socket.emit('getPageLoad', res.data[0].questionAsked)
            console.log('getPageLoadSocket', res.data[0].questionAsked)
        })
        .catch(err =>{
            console.log('Error: ', err.message)
        })
    })
})

// axios.get('http://localhost:3000/questions')
// .then(res =>{
//     console.log(res.data[0].answers[0])
// })
// .catch(err =>{
//     console.log('Error: ', err.message)
// })

// axios({
//     method: 'post',
//     url: process.env.AXIOS_URL + '/questions',
//     data: {
//         questionAsked: "f",
//         answers: [
//             "Auckland",
//             "Wellington"
//         ]
//     }
//   });

server.listen(port, () =>{
    console.log('Server is up on port ' + port);
});